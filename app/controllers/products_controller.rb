class ProductsController < ApplicationController
  require 'net/http'
  require 'json'

  def index
  	@products = Product.all
  end

  def show
  	user_currency_code = current_user.currency_code
  	# Setting URL
  	url = "https://v3.exchangerate-api.com/bulk/dcfba1f6bf16a44d41b181d9/INR"
  	uri = URI(url)
  	response = Net::HTTP.get(uri)
  	response_obj = JSON.parse(response)

  	# Getting rate
  	rate = response_obj['rates'][user_currency_code]

    	@product = Product.find(params[:id].to_i)
    	@product_price = (@product.price * rate).round(2)
    	@currency_code = user_currency_code
  end
end





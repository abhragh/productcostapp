class HomeController < ApplicationController
  require 'unirest'

  def index
  end

  def update_country 
  	country_code = params[:pt]

  	c = ISO3166::Country.new(country_code)
  	currency_code = c.currency['code']
    current_user.update_attributes(:login_country_code => country_code , :currency_code => currency_code)
  end
end


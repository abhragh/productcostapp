module Api
	module V1
		class ProductsController < ApplicationController
			skip_before_action :verify_authenticity_token
			require 'net/http'
  			require 'json'

			def details
				product_name = params[:product_name]
				country_code = params[:country_code]

				if country_code.length == 2  

					c = ISO3166::Country.new(country_code)
					if c.nil?
					  render json:
			          {
			            Status: 'Failure - No such currency exists' 
			          }, status: 400
					else
		  				currency_code = c.currency['code']

		  				product = Product.find_by_name(product_name)

		  				# Setting URL
					  	url = "https://v3.exchangerate-api.com/bulk/dcfba1f6bf16a44d41b181d9/INR"
					  	uri = URI(url)
					  	response = Net::HTTP.get(uri)
					  	response_obj = JSON.parse(response)

					  	# Getting rate
					  	rate = response_obj['rates'][currency_code]

					  	product_description = product.description
					  	product_price = product.price
					  	converted_price = (product_price * rate).round(2)

					  	converted_price_string = converted_price.to_s+" "+currency_code
					  	render json:
				          {
				            Status: 'Success', 
				            Name: product_name,
				            Description: product_description, 
				            Price: converted_price_string
				          }, status: 200
				    end
			    else
			    	render json:
			          {
			            Status: 'Failure - Please provide ISO 2 country codes (2 Letter)' 
			          }, status: 400
			    end
			end
		end
	end
end


 
Rails.application.routes.draw do

  get 'home/index'

  root 'home#index'

  get 'products/index'

  get 'products/show'

  get 'home/update_country' => 'home#update_country'  
  post 'home/update_country' => 'home#update_country'  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root "home#dashboard"

  # devise_scope :user do
	 #  root to: "devise/sessions#new"
  # end
  devise_for :users, controllers: {sessions: "session"}

  namespace :api do
    namespace :v1 do |v1|
      post '/products/details' => 'products#details'
    end
  end
end

class AddCurrencyCodetoUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :currency_code, :string
  end
end

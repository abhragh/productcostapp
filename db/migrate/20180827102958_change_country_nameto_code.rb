class ChangeCountryNametoCode < ActiveRecord::Migration[5.1]
  def change
  	rename_column :users, :login_country_name, :login_country_code
  end
end
